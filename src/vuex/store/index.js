import Vue from 'vue'
import Vuex from 'vuex'
import Api from "../../services/Api";

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        hotels: [],
        cities: [],
        facilities: [],
        apartmentsData: null,
        lastPage: 1
    },
    mutations: {
        SET_HOTELS_DATA(state, data) {
            state.hotels = data;
        },
        SET_CITIES_DATA(state, data) {
            state.cities = data;
        },
        SET_FACILITIES_DATA(state, data) {
            state.facilities = data;
        },
        SET_APARTMENTS_DATA(state, data) {
            state.apartmentsData = data;
        },
        SET_LAST_PAGE(state, data) {
            state.lastPage = data;
        }
    },
    actions: {
        fetchHotels(context) {
            Api.get('hotels')
                .then(res => {
                    context.commit('SET_HOTELS_DATA', res.data.data);
                })
        },
        fetchCities(context) {
            Api.get('cities')
                .then(res => {
                    context.commit('SET_CITIES_DATA', res.data.data);
                })
        },
        fetchFacilities(context) {
            Api.get('facilities')
                .then(res => {
                    context.commit('SET_FACILITIES_DATA', res.data.data);
                })
        },
        fetchApartmentsData(context, query) {
            Api.get('apartments', 'q=' + query)
                .then(res => {
                    context.commit('SET_APARTMENTS_DATA', res.data.data);
                    context.commit('SET_LAST_PAGE', res.data.meta.last_page);
                })
                // eslint-disable-next-line no-unused-vars
                .catch(err => {
                    context.commit('SET_APARTMENTS_DATA', []);
                })
        }
    }
})