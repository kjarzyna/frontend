class QueryParamHelper {
    updateQueryString(searchQuery) {
        let newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?q=' + searchQuery;
        window.history.pushState({path:newUrl},'',newUrl);
    }

    buildSearchQuery(searchData) {
        return JSON.stringify(searchData);
    }

    resolveSearchDataFromQueryParam(fallback) {
        let urlParams = new URLSearchParams(window.location.search);
        let q = urlParams.get('q');
        try {
            return q ? JSON.parse(q): fallback;
            // eslint-disable-next-line no-empty
        } catch (ignored) {}
    }
}

export default new QueryParamHelper();