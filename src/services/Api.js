import axios from 'axios'

class Api {

    constructor() {
        this.apiPath = process.env.VUE_APP_API_URL;
    }

    get(endpoint, query = '') {
        let path = this.apiPath + '/' + endpoint + '?' + query;

        return axios.get(path);
    }
}

export default new Api();